import * as deepmerge from 'deepmerge'
import {fromJS} from 'immutable'
import * as fs from 'fs-extra'
import * as minimatch from 'minimatch'
import * as path from 'path'

export interface IGetNotesFromDirectoryNoteItem {
	comment: string
	content: string
	filePath: string
	lineNumber: number
	position: number
	user: string
}

export interface IGetNotesFromDirectoryOptions {
	ignoreFiles?: string[]
	ignorePattern?: string
	rootDirectory?: string
	sessions?: IGetNotesFromDirectoryReturnData
	verbose?: boolean
	watch?: boolean
}

export interface IGetNotesFromDirectorySessionItem {
	notes: {[noteId: string]: IGetNotesFromDirectoryNoteItem}
	timestamp: number
}

export interface IGetNotesFromDirectoryReturnData {
	[sessionId: string]: IGetNotesFromDirectorySessionItem
}

const
	masterRegex = new RegExp(/\*\s#CN_(\d+);(\d+);(.+);start(.|\n)+#CN_\1;.+;end/g),
	noteInfoRegex = new RegExp(/\*\s#CN_(\d+);(\d+);(.+);start/),
	commentSectionRegex = new RegExp(/(.|\n)+?\*\//),
	commentLineStartRegex = new RegExp(/^.+\*/gm),
	commentSectionEndRegex = new RegExp(/.+\*\//),
	noteEndRegex = new RegExp(/\/\*+\n.+\*\s#CN_(\d+);.+;end/)
/**
 * Read a directory and its contents recursively and parse the files inside for CN comments.
 * @param pathToDirectory (required) - The pathToDirectory to the directory.
 * @param options (optional)
 * @returns The string or object with the changed keys.
 */
export async function getNotesFromDirectory(
	pathToDirectory: string,
	options?: IGetNotesFromDirectoryOptions
): Promise<IGetNotesFromDirectoryReturnData> {
	let opt = fromJS(options || {}).toJS() as IGetNotesFromDirectoryOptions
	const {ignoreFiles, ignorePattern, verbose} = opt,
		logMethod = verbose === true ? console.log : () => true
	if (!opt.rootDirectory) {
		opt.rootDirectory = pathToDirectory
	}
	if (!opt.sessions) {
		opt.sessions = {}
	}
	logMethod(`[codeNotes][${pathToDirectory}] Opending directory...`)
	let dirData = await fs.readdir(pathToDirectory),
		sessions = opt.sessions
		logMethod(`[codeNotes][${pathToDirectory}] Found ${dirData.length} children.`)
	for (const i in dirData) {
		let fileName = dirData[i]
		let pathToFile = path.join(pathToDirectory, fileName),
			relativePathToFile = pathToFile.replace(opt.rootDirectory as string, '')
		relativePathToFile = relativePathToFile.substr(1, relativePathToFile.length)
		if (ignoreFiles) {
			let ignored = false
			for (const j in ignoreFiles) {
				if (minimatch(relativePathToFile, ignoreFiles[j])) {
					ignored = true
					break
				}
			}
			if (ignored) {
				logMethod(`[codeNotes][${pathToDirectory}] Ignoring child ${fileName}.`)
				continue
			}
		} 
		if (ignorePattern && ignorePattern.match(fileName)) {
			logMethod(`[codeNotes][${pathToDirectory}] Ignoring child ${fileName}.`)
			continue
		}
		let stats = await fs.lstat(pathToFile)
		if (stats.isDirectory()) {
			sessions = deepmerge(sessions, await getNotesFromDirectory(pathToFile, opt))
			continue
		}
		let fileData = (await fs.readFile(pathToFile)).toString(),
			matches = fileData.match(masterRegex)
		if (matches && matches.length) {
			logMethod(`[codeNotes][${pathToDirectory}] Found ${matches.length} notes for child ${fileName}, processing...`)
			let currentLineNumber = (fileData.substr(0, fileData.indexOf(matches[0])).match(/\n/g) as RegExpMatchArray).length
			matches.forEach((item: string) => {
				let noteInfo = (noteInfoRegex.exec(item) as RegExpMatchArray),
					commentSection = (commentSectionRegex.exec(item) as RegExpMatchArray)[0],
					commentSectionContent = commentSection
						.replace(noteInfo[0], '')
						.replace(commentLineStartRegex, '')
						.replace(commentSectionEndRegex, ''),
					noteContent = item.replace(commentSection, '').replace(noteEndRegex, ''),
					sessionId = noteInfo[2]
				if (!sessions[sessionId]) {
					sessions[sessionId] = {notes: {}, timestamp: parseInt(sessionId, 10)}
				}
				sessions[sessionId].notes[noteInfo[1]] = {
					comment: commentSectionContent,
					content: noteContent,
					filePath: relativePathToFile,
					lineNumber: currentLineNumber,
					position: fileData.indexOf(item),
					user: noteInfo[3]
				}
				currentLineNumber += (item.match(/\n/g) as RegExpMatchArray).length
			})
			logMethod(`[codeNotes][${pathToDirectory}] Processing child ${fileName} completed.`)
			continue
		}
		logMethod(`[codeNotes][${pathToDirectory}] No notes found for child ${fileName}...`)
	}
	return sessions
}
