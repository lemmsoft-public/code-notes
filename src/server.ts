#!/usr/bin/env node

import * as express from 'express'
import * as fs from 'fs-extra'
import {getNotesFromDirectory} from './scripts'
import * as http from 'http'
import * as moment from 'moment'
import * as path from 'path'
import * as pug from 'pug'
import * as yargs from 'yargs'

const argv = yargs.argv

if (argv.h || argv.help) {
	console.log(
		`Code notes - A tool for creating and displaying code annotations.\n` +
		`  Usage: code-notes -p 9001\n` +
		`    This will run a server at port 9001, on which you can see the code notes for the current directory and its children.\n` +
		`\n` +
		`  There are a number of avilable options:\n` +
		`    -p, --serverPort=   (required) the port at which to run the server\n` +
		`    -d, --dir=          (optional) specify the root directory; if not provided, the current working directory is used\n` +
		`    -v, --verbose=          (optional) display detailed execution logs\n` +
		`\n` +
		`  You can place a ".codenotesignore" file with the same glob syntax as a typical ".gitignore" file to exclude files and folders from the search.\n`
	)
	process.exit(0)
}

(async function() {
	const verboseMode = argv.v || argv.verbose ? true : false,
		workDir = (argv.d as string) || (argv.dir as string) || process.cwd()
	let app = express(),
		getLayout = pug.compile((await fs.readFile(path.join(__dirname, './client/layout.pug'))).toString()),
		getSessionBlock = pug.compile((await fs.readFile(path.join(__dirname, './client/sessionBlock.pug'))).toString()),
		getNoteBlock = pug.compile((await fs.readFile(path.join(__dirname, './client/noteBlock.pug'))).toString())
	app.get(
		'/',
		(_req, res) => {
			(async function() {
				console.log(`[codeNotes] Reading root directory...`)
				let ignoreFiles: string[] = []
				try {
					ignoreFiles = (await fs.readFile(path.join('.codenotesignore'))).toString().split(/\n/)
				} catch(e) {}
				let sessions = await getNotesFromDirectory(workDir, {ignoreFiles, verbose: verboseMode}),
					data = ''
				for (const sessionId in sessions) {
					let {notes, timestamp} = sessions[sessionId],
						sessionNotes = ''
					for (const noteId in notes) {
						let noteData = notes[noteId]
						sessionNotes += getNoteBlock({
							comments: noteData.comment.replace(/\n/g, '<br/>').replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;'),
							content: noteData.content.replace(/\n/g, '<br/>').replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;'),
							fileName: noteData.filePath,
							lineNumber: noteData.lineNumber,
							noteId,
							position: noteData.position,
							user: noteData.user
						})
					}
					data += getSessionBlock({
						notes: sessionNotes,
						sessionDate: `${moment(timestamp, 'x').format('YYYY-MM-DD HH:mm')} GMT`,
						sessionId
					})
				}
				console.log(`[codeNotes] Root directory read completed.`)
				res.send(getLayout({data}))
			})().then(
				() => true,
				(err) => {
					console.error(err)
					res.status(500).json({error: err})
				}
			)
		}
	)
	let server = http.createServer(app)
	await new Promise((resolve) => {
		server.listen(argv.p || argv.serverPort, () => resolve(true))
	})
})().then(
	() => {
		console.log(`[codeNotes] Server started.`)
		console.log(`[codeNotes] Port:`, argv.p || argv.serverPort)
	},
	(err) => console.error(err)
)
