const
	path = require('path'),
	{getNotesFromDirectory} = require('../dist')

getNotesFromDirectory(path.join(__dirname, 'content')).then((data) => console.log(data))
