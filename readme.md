![npm](https://img.shields.io/npm/v/@lemmsoft/code-notes.svg)
<br/>
Code Notes
==
Code notes - A tool for creating and displaying code annotations.<br/>
&nbsp;&nbsp;Usage: `code-notes -p 9001`<br/>
&nbsp;&nbsp;&nbsp;&nbsp;This will run a server at port 9001, on which you can see the code notes for the current directory and its children.<br/>
<br/>
&nbsp;&nbsp;There are a number of avilable options:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;-p, --serverPort=&nbsp;&nbsp; (required) the port at which to run the server<br/>
&nbsp;&nbsp;&nbsp;&nbsp;-d, --dir=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(optional) specify the root directory; if not provided, the current working directory is used<br/>
&nbsp;&nbsp;&nbsp;&nbsp;-v, --verbose=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(optional) display detailed execution logs<br/>
<br/>
&nbsp;&nbsp;You can place a ".codenotesignore" file with the same syntax as a typical ".gitignore" file to exclude files and folders from the search.\n`
