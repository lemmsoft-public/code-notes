# 0.4.1
- Added the file name to the note info block in the page.

# 0.4.0
- Improved the readme and help command.
- Added a verbose mode.
- Vastly improved the ignore matching pattern using minmatch.
- Fixed the bug with nested folders.

# 0.3.1
- Small fix.

# 0.3.0
- Removed the postinstall script and added a shebang.

# 0.2.1
- Postinstall script logging improvements.

# 0.2.0
- Proper postinstall fix.

# 0.1.1
- Readme update.
- Postinstall script fix.

# 0.1.0
- Initial version.
